﻿$(".slider").slick({
	prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
	nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
	responsive: [{
		breakpoint: 1025,
		settings: {
			arrows: false
		}
	}]
});

$(".product__slide").slick({
	prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
	nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
	slidesToShow: 4,
	responsive: [{
		breakpoint: 1025,
		settings: {
			arrows: false,
			slidesToShow: 3
		}

	}, {
		breakpoint: 600,
		settings: {
			arrows: false,
			slidesToShow: 1
		}
	}]
});

// event click to scroll top
$(".footer__scroll").click(function (event) {
	event.preventDefault();
	$('html, body').animate({ scrollTop: 0 }, 500);
	return false;
})

// event click to show menu mobile
$("#showMenu").click(function (e) {
	e.preventDefault();
	$("#nav").css("left", "20px");
	$("#overlay").css({
		'visibility': 'visible',
		'opacity': '1'
	})
	return false;
})

// event click to hide menu mobile
$("#overlay").click(function (e) {
	e.preventDefault();
	$("#nav").css("left", "-320px");
	$("#overlay").css({
		'visibility': 'hidden',
		'opacity': '0'
	})
})